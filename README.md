# v-rising-wine

## AWS Lightsail
Open UDP Ports (if you don't change the default 9876,9877) for the instance
Create a static IP Adress for your instance
## Installing docker on AWS Lightsail
``sudo yum update``

---

Install docker

``sudo yum install docker``

---

Add ec2-user to docker group

``sudo usermod -a -G docker ec2-user``

---

Install docker-compose

``sudo pip3 install docker-compose``

---

Add docker to start everytime the instance is started

``sudo systemctl enable docker.service``

---

Start the server

``sudo systemctl start docker.service``

---

Check if service is running

``sudo systemctl status docker.service``

---

Reboot

``sudo reboot``

---

After the server is back up again check if service is running

``sudo systemctl status docker.service``


## Starting V Rising server
``mkdir saves``

---

``wget https://gitlab.com/projectmagaming/v-rising-wine/-/raw/main/docker-compose.yaml``

---

``vi docker-compose.yaml``

---

Change the setings to your liking

``docker-compose up``


| Parameter      | Default Value | Options |
| ----------- | ----------- | ----------- |
| V_RISING_NAME       |        |Mandatory Name of the server|
| V_RISING_PASSW    |         |Password for the Server|
| V_RISING_SAVE_NAME    |    world1     |Mandatory Name of the save file|
| V_RISING_PUBLIC_LIST    | true        ||
| V_RISING_DESC   |         ||
| V_RISING_SETTING_PRESET   |       ||


| Parameter      | Default Value | Options |
| ----------- | ----------- | ----------- |
| V_RISING_MAX_HEALTH_MOD      | 1.0       ||
| V_RISING_MAX_HEALTH_GLOBAL_MOD   | 1.0        ||
| V_RISING_RESOURCE_YIELD_MOD   | 1.0        ||
| V_RISING_DAY_DURATION_SECONDS   | 1080.0        ||
| V_RISING_DAY_START_HOUR   | 9       ||
| V_RISING_DAY_END_HOUR   | 17       ||
| V_RISING_TOMB_LIMIT   | 4        ||
| V_RISING_NEST_LIMIT   | 12        ||
| V_RISING_PORT   | 9876       ||
| V_RISING_QUERY_PORT   | 9877        ||
| V_RISING_MAX_USER   | 40        ||
| V_RISING_CLAN_SIZE   | 4        ||
| V_RISING_DEATH_CONTAINER_PERMISSIONS   | Anyone       | Anyone, ClanMembers
| V_RISING_GAME_MODE   | PvP       | PvP, PvE

